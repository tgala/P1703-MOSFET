
disp("=====================================");
disp("Script de tratamento de dados picolog");
disp("=====================================");

disp("\n\tINPUT: temperature_log.csv");
disp("\tATENÇÃO: remover cabeçalhos, substituir virgulas por pontos");

disp("=====================================");

INPUT = "temperature_log.csv";

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CABEÇALHO: (tempo em segundos, temperaturas em ºC)
%   TIME
%   T_AMBIENTE
%   MOSFET_BOT
%   PCB__TOP
%   HOTBED
%   Tbloco      ???
%   MOSFET_TOP
%   T_AMBIENTE  ???
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = dlmread(INPUT, "\t");

time  = data(:,1);
t_amb = data(:,2);
fet_b = data(:,3);
pcb_t = data(:,4);
bed_t = data(:,5);
block = data(:,6);
fet_t = data(:,7);
t_amb = data(:,8);

graphics_toolkit("gnuplot");

t=time/60;

plot(t, pcb_t, t, t_amb, t, fet_b, t, pcb_t, t, bed_t, t, block, t,fet_t, t, t_amb);

% falta tratar o fet_t
% falta ajustar escalas
% falta adicionar legendas